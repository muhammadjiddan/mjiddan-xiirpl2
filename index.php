<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/sytle.css">
    <title>Hello, world!</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
            <img src="img/LOGO MGW.png" width="100" height="100" class="d-inline-block align-top">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Courses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                    <li>
                        <a href="login/index.php" class="btn btn-primary">Log In</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-11">
                    <h1 class="display-4 col-sm-8">Жизнь без испытаний и выборов - пустая жизнь
                        Zhizn' bez ispytaniy i vyborov - pustaya zhizn'</h1>
                    <p class="lead">Hidup tanpa ujian dan pilihan adalah sebuah hidup yang kosong</p>
                    <button class="btn btn-primary">Get Access</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Jumbotron -->

    <!-- Service -->
    <section class="service text-center d-flex align-items-center justify-content-center" id="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-5">
                    <h4>Service</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <img src="img/service1.svg" alt="">
                    <h6>Unlimited Access</h6>
                    <p>One subscruotion unlimited access</p>
                </div>
                <div class="col-lg-4">
                    <img src="img/service2.svg" alt="">
                    <h6>Expert Teachers</h6>
                    <p>learn from industry experts who are passionate about teaching</p>
                </div>
                <div class="col-lg-4">
                    <img src="img/service3.svg" alt="">
                    <h6>Learn Anywhere</h6>
                    <p>Switch between your computer, tablet, or mobile device</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Akhir Service -->

    <!-- Stories -->
    <section class="stories d-flex align-items-center" id="stories">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h1>Success Stories From Our Students WorldWide!</h1>
                    <p>Semaj Africa is an online education platform that delivers video courses, programs and resource
                        for Individual. Advertising & Media Specialist, Online Marketing Professionals, Freelancers and
                        anyone. looking to pursue a career in digital marketing, Accounting, Web Development,
                        Programming Multimedia and CAD design
                    </p>
                    <button class="btn btn-primary">Discover</button>
                </div>
                <div class="col-lg-8 text-center">
                    <img src="img/stories-img.png" width="80%" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Akhir Stories -->

    <!-- Courses -->
    <section class="courses d-flex align-items-center" id="courses">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h4>Courses</h4>
                </div>
            </div>

            <div class="row pb-4">
                <div class="col-lg-12 text-center">
                    <a href="">All</a>
                    <a href="">Design</a>
                    <a href="">Web Development</a>
                    <a href="">Digital</a>
                    <a href="">Photography</a>
                    <a href="">Motion Graphics</a>
                    <a href="">Digial Marketing</a>
                </div>
            </div>

            <div class="row">
            

            <?php
            include "koneksi.php";
            $data = mysqli_query($koneksi, "select*from courses");
            while($d = mysqli_fetch_array($data)) {
            ?>
            <div class="col-lg-3">
                <div class="card">
                  <div class="card-top text-center pt-2">
                    <?php echo $d['princing'] ?>
                  </div>
      
                  <img src="img/<?php echo $d['img']?>" class="card-img-top" alt="...">
                  <div class="card-body">
                    <p><?php echo $d['description'] ?></p>
                  </div>
                </div>
              </div>


            <?php
            }
            ?>
            </div>

            <div class="row justify-content-center mt-5">
                <button class="btn btn-primary">Discover</button>
            </div>

        </div>
    </section>
    <!-- Akhir Courses -->

    <!-- Newsletter -->
    <section class="newsletter d-flex align-items-center" id="newsletter">
        <div class="container">
            <img src="img/Rectangle7.svg" alt="" class="imgs">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-8">
                    <h4 class="display-h4">Subscribe to Our Newsletter</h4>
                    <p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
                    <button class="btn btn-primary">Start free trial</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Akhir Newsletter -->

    <!-- Contact -->
    <section class="contact d-flex align-items-center" id="contact">
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <img src="img/Logo.svg" alt="" class="mb-3">
                <p>Semaj Africa is an online education platfrom that delivers video courses, programs and resources.</p>
                <img src="img/sosmed.svg" alt="" class="mt-5">
            </div>
            <div class="col-lg-2">
                <h6>Quicklinks</h6>
                <ul>
                    <li>Home</li>
                    <li>Courses</li>
                    <li>About Us</li>
                    <li>Contact Us</li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h6>Contact Us</h6>
                <ul>
                    <li>(+55) 254. 254. 254</li>
                    <li>Info@lsemajafrica.com</li>
                    <li>Helios Tower 75 Tam Trinh Hoang</li>
                    <li>Mai - Ha Noi - Viet Nam</li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h6>Terms and Conditions Faq</h6>
            </div>
        </div>
    </section>
    <!-- Akhir Contact -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>