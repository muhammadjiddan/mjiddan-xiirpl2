<?php

session_start();
session_destroy();

setcookie('id', '', time()-60);
setcookie('username', '', time()-60);
header("Location:../login/index.php");
exit;
?>